# Collection of `.gitlab-ci.yml` templates

This is a collection of [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/README.html) file templates.

Files can be included in your `.gitlab-ci.yml` using the `include` keyword:

```yaml
include:
  - project: 'tango-controls/gitlab-ci-templates'
    file: 'Docker.gitlab-ci.yml'
```

See <https://docs.gitlab.com/ee/ci/yaml/#include> for more information.

## Available templates

### `ArchiveWithSubmodules.gitlab-ci.yml`

When creating a release, GitLab automatically creates an archive of the repository, but it doesn't include submodules.

Include this template to create an extra archive that will include git submodules.
The archive is uploaded to the repository generic package registry and a link is added to the release.

If a release is created from GitLab UI at the same time as the tag, the link
is added to that release. If the release doesn't exist when taggging, it is
automatically created. You can edit it later.

### `Docker.gitlab-ci.yml`

Template to build docker images:

* On tag, publish the image with `latest` and the git tag
* On push, publish the image with the `branch` name as tag

### `Java.gitlab-ci.yml`

Template to build and deploy java applications:
- On push, build, test application and optionally verify code by using SonarCloud
- On tag, create artifacts and empty release tab on GitLab and then create manual stage with deploy

Flags to set:
- `ENABLE_SONARCLOUD`, run stage with Sonarcloud analysis (default: false)
- `ENABLE_TANGO_DB`, attach minimal tango-cs setup to the build stage (default: false)
- `DEPLOY_JAVA_IMAGE`, image for deployment and Sonar-analysis stage (default: maven:3.8.4-jdk-8-slim)
- `DEPLOY_JAVA_VERSION`, version of Java used in DEPLOY_JAVA_IMAGE (default: 1.8)
- `ENABLE_JAVA_8`, run build stage with Java8 (default: true)
- `ENABLE_JAVA_11`, run build stage with Java11 (default: false)
- `ENABLE_JAVA_17`, run build stage with Java17 (default: false)

`.gitlab-ci.yml` example:
```yaml
include:
  - project: 'tango-controls/gitlab-ci-templates'
    ref: main
    file: 'Java.gitlab-ci.yml'
variables:
  ENABLE_TANGO_DB: "true"
  ENABLE_JAVA_8: "false"
  ENABLE_JAVA_11: "true"
  ENABLE_JAVA_17: "true"
```

### `cpp.gitlab-ci.yml`

Template for CI for C++ device servers and clients:

Flags to set:
  - `CMAKE_BUILD_TYPE`: One of Release, Debug, RelWithDebInfo
  - `TANGO_OS_TYPE`: debian12
  - `TANGO_WARNINGS_AS_ERRORS`: ON

`.gitlab-ci.yml` example:
```yaml
include:
  - project: 'tango-controls/gitlab-ci-templates'
    ref: main
    file: 'cpp.gitlab-ci.yml'
```

### `PreCommit.gitlab-ci.yml`

Template to run `pre-commit` (when `.pre-commit-config.yaml` exists).

### `PythonPublic.gitlab-ci.yml`

Template to build and publish a Python package to PyPI.
Pytest is also run if tests are found.

The following variables can be used:

* `PYTEST_ADDOPTS` to pass extra arguments to pytest
* `TEST_WITH_TANGO_DB` can be set to `true` to run a tango database during the tests - by default no db available

`.gitlab-ci.yml` example:

```yaml
include:
  - project: 'tango-controls/gitlab-ci-templates'
    file: 'PythonPublic.gitlab-ci.yml'
```
